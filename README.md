
A collection of webcomics [fortune][1] cookies. It is based on quotes
from the following sources.

* [xkcd][2]
* [Abstruse Goose][3]

[1]: http://manpages.ubuntu.com/manpages/trusty/en/man6/fortune.6.html
[2]: http://xkcd.com/
[3]: http://abstrusegoose.com/
