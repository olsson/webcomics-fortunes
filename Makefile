
fortunes := xkcd abstruse
datfiles := $(shell echo $(fortunes) | sed -re "s/([a-z]+\b)/\1.dat/g")

build: $(datfiles)

%.dat: %
	strfile $<

install: build
	install -m 644 $(fortunes) /usr/share/games/fortunes/
	install -m 644 $(datfiles) /usr/share/games/fortunes/

.PHONY: install build
